import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelMenuModule } from 'primeng/panelmenu';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { AutoCompleteModule } from 'primeng/autocomplete';


import { ReactiveFormsModule } from '@angular/forms'; 


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    PanelMenuModule,
    CardModule,
    InputTextModule,
    ButtonModule,
    ReactiveFormsModule,
    RadioButtonModule,
    DropdownModule,
    FileUploadModule,
    CalendarModule,
    TableModule,
    DialogModule,
    DynamicDialogModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    ToastModule
  ],
  exports:[
    PanelMenuModule,
    CardModule,
    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    DropdownModule,
    FileUploadModule,
    CalendarModule,
    TableModule,
    DialogModule,
    DynamicDialogModule,
    ConfirmDialogModule,
    ToastModule,
    AutoCompleteModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
