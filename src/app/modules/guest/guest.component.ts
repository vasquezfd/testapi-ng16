import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-guest-layout',
  standalone: true,
  imports: [
    CommonModule,
  ],
  template: `<p>guestLayout works!</p>`,
  styleUrls: ['./guest.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GuestComponent { }
