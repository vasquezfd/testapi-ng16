import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminComponent } from './admin.component';
import { SidebarComponent } from './presentation/components/sidebar/sidebar.component';
import { HeaderComponent } from './presentation/components/header/header.component';
import { NavbarComponent } from './presentation/components/navbar/navbar.component';
import { FooterComponent } from './presentation/components/footer/footer.component';
import { AlumnoComponent } from './presentation/pages/alumno/alumno.component';
import { AlumnoFrmComponent } from './presentation/pages/alumno-frm/alumno-frm.component';
import { RepositoryService } from './core/repositories';
import { StoreModule } from '@ngrx/store';
import { ADMIN_EFFECTS, AdminAppState, MAESTROS_CRUD_KEY_FEATURE } from './presentation/state';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [
    AdminComponent,
    SidebarComponent,
    HeaderComponent,
    NavbarComponent,
    FooterComponent,

    AlumnoComponent,
    AlumnoFrmComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    StoreModule.forFeature(MAESTROS_CRUD_KEY_FEATURE, AdminAppState.maestrosCrud),
    EffectsModule.forFeature(ADMIN_EFFECTS),
  ],
  providers: [
    ...RepositoryService.providers('http'),
  ]
})
export class AdminModule { }
