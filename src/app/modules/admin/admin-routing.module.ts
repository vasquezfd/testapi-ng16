import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnoComponent } from './presentation/pages/alumno/alumno.component';

const routes: Routes = [
  { 
    path: 'alumno/lista',
    component: AlumnoComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
