import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuItem, PrimeIcons } from 'primeng/api';

@Component({
  selector: 'app-sidebar',
  template: `
    <p-panelMenu class="panel-menu" [model]="items" [style]="{'width':'300px'}"></p-panelMenu>
  `,
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidebarComponent { 
  items: Array<MenuItem> = [
    {
      label: 'Alumno',
      expanded: true,
      icon: PrimeIcons.USER,
      items:[
        {
          label: 'Listar',
          routerLink: 'alumno/lista',
          icon: PrimeIcons.USERS
        },
      ]
    }
  ]
}
