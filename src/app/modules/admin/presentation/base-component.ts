import _ from "lodash";
import { DateTime } from "luxon";
import { BehaviorSubject, Subscription } from "rxjs";
import { escapeObject } from "../services/utils";
// import { ComponentCanDeactivate } from "./components/can-deactivate/component-can-deactivate";
// import { AuthService } from "src/app/core/services";
import { Inject } from "@angular/core";
import { AppModule } from "src/app/app.module";

export class BaseComponent {
//   _local: Local;
  _subs: Array<Subscription | undefined> = [];
  _loading = new BehaviorSubject<boolean>(false);
//   _authService: AuthService;
  
  constructor(){}

//   translate(text: string, option = 'm', paramTrans: unknown = {}): string {
//     return this._translate.instant(this.local(text, option), escapeObject(paramTrans));
//   }

  unsubscribe(){
    this._subs.forEach(subscription => {
      if( subscription !== undefined ){
          subscription.unsubscribe();
      }
    });
  }

//   local(text:string, option = 'm'){
//     return this._local.get(text, option)
//   }

//   canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
//     return true;
//   }

  escape(text:string): string{
    return _.escape(text)
  }

  getDateLocaleString(date: Date, customMessage: string=''): string{
    const fecha = DateTime.fromJSDate(date)
    return fecha.isValid
      ? fecha.toLocaleString(DateTime.DATE_HUGE)
      : customMessage.length > 0 ? customMessage : ''
  }

//   isUserLogged(): boolean{
//     return this._authService.userIsLogged
//   }
}