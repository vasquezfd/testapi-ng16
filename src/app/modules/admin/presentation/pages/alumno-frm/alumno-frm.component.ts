import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AdminState, AlumnoActions, PaisActions, TipoDocumentoActions } from '../../state';
import { BaseComponent } from '../../base-component';
import { Observable, first, filter, from, switchMap, of, mergeMap, concatMap, merge, map, concat, mergeAll } from 'rxjs';
import { Alumno, Pais, TipoDoc } from '../../../core/domain';
import { MaestrosCrudConfigSelect as selectMaestro } from '../../state/selectors';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteCompleteEvent } from 'primeng/autocomplete';
import { compareString } from '../../../services/utils';

export enum FRM_STATE {
  NEW,
  EDIT
}

interface nacionalidad {
  name: string,
  cca3: string,
}

@Component({
  selector: 'app-alumno-frm',
  templateUrl: './alumno-frm.component.html',
  styleUrls: ['./alumno-frm.component.scss'],
  providers:[ConfirmationService, MessageService]
})

export class AlumnoFrmComponent extends BaseComponent {
  alumno$: Observable<Alumno>
  loading$: Observable<boolean>
  mode: FRM_STATE = FRM_STATE.NEW

  title = 'Registrar un nuevo Alumno'
  lstTipoDocIdentidad$: Observable<TipoDoc[]>
  lstNacionalidades$: Observable<Pais[]>
  lstNacionalidadesFilter: string[] = []

  alumnoform = this.fb.group({
    idAlumno:[0 ],
    nombres: ['', [Validators.required, Validators.maxLength(50)]],
    apellidos: ['', [Validators.required, Validators.maxLength(50)]],
    email: ['', [Validators.required, Validators.email, Validators.maxLength(50)]],
    idTipoDocumento: [1, [Validators.required,]],
    numeroDocumento: ['', [Validators.required, Validators.maxLength(20)]],
    fecNacimiento: [ new Date(), [Validators.required,]],
    sexo: ['MASCULINO', [Validators.required]],
    telefono: ['',[ Validators.maxLength(50)]],
    nacionalidad: ['', [Validators.required, Validators.maxLength(50)]],
  })

  constructor(
    private readonly store: Store<AdminState>,
    private fb: FormBuilder,
    public dialogConfig: DynamicDialogConfig,
    public dialogRef: DynamicDialogRef,
    private confirmationService: ConfirmationService
  ){
      super()
      this.alumno$ = this.store.select(selectMaestro.selectAlumnoMaestro.obtener)
      this.loading$ = this.store.select(selectMaestro.selectAlumnoMaestro.cargando)
      this.lstTipoDocIdentidad$ = this.store.select(selectMaestro.selectTipoDocumento.select)
      this.lstNacionalidades$ = this.store.select(selectMaestro.selectPais.select)
  }

  ngOnInit(){
      
      this.alumnoform.reset()
      const alumno: Alumno | null = this.dialogConfig.data
      if(alumno){
        this.mode = FRM_STATE.EDIT
        this._subs.push(
          this.alumno$.subscribe((alumno) => {
            if(alumno){
              this.alumnoform.setValue({
                idAlumno: alumno.idAlumno,
                nombres: alumno.nombres,
                apellidos: alumno.apellidos,
                email: alumno.email,
                idTipoDocumento: alumno.idTipoDocumento,
                numeroDocumento: alumno.numeroDocumento,
                fecNacimiento: alumno.fecNacimiento,
                sexo: alumno.sexo,
                telefono: alumno.telefono,
                nacionalidad: alumno.nacionalidad,
              })
            }
          })
        )
        setTimeout(() => {
          this.store.dispatch( AlumnoActions.Obtener({id: alumno.idAlumno}))
        }, 0);
      }
      this.store.dispatch(PaisActions.Listar())
      this.store.dispatch( TipoDocumentoActions.Listar() )
  }

  ngOnDestroy(){
    console.log("destroying child...")
    this.unsubscribe();
  }

  addAlumno(){
    if(this.alumnoform.valid){
      this.confirmationService.confirm({
        message: '¿Deseas continuar?',
        header: 'Confirmación',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            switch(this.mode){
              case FRM_STATE.NEW:
                this.store.dispatch(AlumnoActions.Guardar({item: this.alumnoform.value, metadata: {mode: this.mode} }))
                break;
              case FRM_STATE.EDIT:
                this.store.dispatch(AlumnoActions.Actualizar({item: this.alumnoform.value, metadata: {mode: this.mode} }))
                break;
            }
        },
    });

      // this.dialogRef.close(this.alumnoform.value)
    }
  }

  filterCountry(event: AutoCompleteCompleteEvent){
    this.lstNacionalidadesFilter = []
    this.lstNacionalidades$
      .pipe(
        first(),
        switchMap( pais => from(pais).pipe(
          filter((pais, index) => {
            return compareString(pais.name.common, event.query)
          }),
          map(pais => pais.name.common)
        )),
      )
      .subscribe( (pais) => {
        this.lstNacionalidadesFilter.push(pais)
      })
  }

  onUpload(evt: any){

  }
}
