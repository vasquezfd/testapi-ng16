import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumnoFrmComponent } from './alumno-frm.component';

describe('AlumnoFrmComponent', () => {
  let component: AlumnoFrmComponent;
  let fixture: ComponentFixture<AlumnoFrmComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlumnoFrmComponent]
    });
    fixture = TestBed.createComponent(AlumnoFrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
