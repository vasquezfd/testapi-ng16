import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AdminState, AlumnoActions, TipoDocumentoActions } from '../../state';
import { Observable } from 'rxjs';
import { ActionSelector } from '../../state/selectors';
import { Alumno } from '../../../core/domain';
import { MaestrosCrudConfigSelect as selectMaestro } from '../../state/selectors';
import { BaseComponent } from '../../base-component';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AlumnoFrmComponent } from '../alumno-frm/alumno-frm.component';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.scss'],
  providers: [DialogService,MessageService,ConfirmationService]
})
export class AlumnoComponent extends BaseComponent {
    title = "Listado de Alumnos"
    refFrm: DynamicDialogRef | undefined;
    actionAlumno$: Observable<ActionSelector<Alumno>>
    loadingAlumno$: Observable<boolean>
    sourceAlumno: Array<Alumno> = []

    constructor(
      private readonly store: Store<AdminState>,
      public dialogService: DialogService,
      public messageService: MessageService,
      public confirmationService: ConfirmationService,
    ){
      super()
      this.loadingAlumno$ = this.store.select(selectMaestro.selectAlumnoMaestro.cargando)
      this.actionAlumno$ = this.store.select(selectMaestro.selectAlumnoMaestro.action)
    }

    ngOnInit(){
      console.log('AlumnoComponente Init')

      this._subs.push(
        this.actionAlumno$
          .subscribe((action) => {
            switch(action.action){
              case 'LISTADO':
                // console.log(action)
                this.sourceAlumno = action.items
                break;
              case 'OBTENIDO':
                
                break;
              case 'LISTADO_ERROR':
                this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se pudo cargar la Lista de Alumnos' });
                break;
              case 'GUARDADO':
                this.refFrm?.close()
                this.messageService.add({ severity: 'info', summary: 'Confirmado', detail: 'Usuario Registrado' });
                this.store.dispatch(AlumnoActions.Listar())
                break;
              case 'ACTUALIZADO':
                // console.log(action)
                this.refFrm?.close()
                this.messageService.add({ severity: 'info', summary: 'Confirmado', detail: 'Usuario Actualizado' });
                this.store.dispatch(AlumnoActions.Listar())
                break;
              case 'GUARDADO_ERROR':
              case 'ACTUALIZADO_ERROR':
                this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se pudo completar la operación' });
                break;
              case 'ELIMINADO':
                this.messageService.add({ severity: 'info', summary: 'Confirmado', detail: 'Usuario Eliminado' });
                this.store.dispatch(AlumnoActions.Listar())
                break;
              case 'ELIMINADO_ERROR':
                // console.log(action)
            }
          })
      )

      this.store.dispatch( AlumnoActions.Listar() )
    }

    ngOnDestroy(){
      console.log("destroying child...")
      this.unsubscribe();
    }

    openDialog(header:string, data?:any){
      this.refFrm = this.dialogService.open(AlumnoFrmComponent, { 
          header,
          data,
      })
    }

    onNew(){
      this.openDialog('Registrar información del Alumno')
    }

    onEdit(alumno: Alumno){
      this.openDialog('Editar información del Alumno', alumno)
    }

    onDelete(alumno: Alumno){
      this.confirmationService.confirm({
        message: '¿Deseas eliminar el registro?',
        header: 'Eliminación',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.store.dispatch(AlumnoActions.Eliminar({item: alumno}))
        }      
      })
    }

    onSearch(){
      this.store.dispatch(AlumnoActions.Listar())
    }
}
