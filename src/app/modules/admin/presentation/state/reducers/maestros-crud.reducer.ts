import { ActionReducerMap } from "@ngrx/store";
import { AlumnoActions, PaisActions, TipoDocumentoActions } from "../actions";
import { MaestrosCrudAppState, initialAlumnoMaestroState, initialPaisState, initialTipoDocumentoState } from "../maestros-crud.state";
import { crudCreateReducer, execCreateReducer } from "./basic-reducer";

export const MaestrosCrudReducer: ActionReducerMap<MaestrosCrudAppState> = {
  alumno: crudCreateReducer(initialAlumnoMaestroState, AlumnoActions),
  pais: execCreateReducer(initialPaisState, PaisActions),
  tipoDocumento: execCreateReducer(initialTipoDocumentoState, TipoDocumentoActions),
}