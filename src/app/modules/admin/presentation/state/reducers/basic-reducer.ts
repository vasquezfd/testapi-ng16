/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action, ActionCreator, ActionReducer, createReducer, on, ReducerTypes } from "@ngrx/store";
import { TypedAction } from "@ngrx/store/src/models";
import { CrudActions, ExecActions } from "../actions";

export function execCreateReducer<T>(initialState: any, actions: ExecActions<T>, ...ons: ReducerTypes<any, readonly ActionCreator[]>[]): ActionReducer<any, Action> {
  return createReducer(
    initialState,
    on(<any>actions.Reset, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      items:[],
      item: null,
      metadata: null,
      action:'',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.ResetItem, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      item: null,
      metadata: null,
      action:'',
    })),
    on(<any>actions.ResetItems, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      items:[],
      metadata: null,
      action:'',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.Listar, (state) => ({
      ...state,
      loading: true,
      loaded: false,
      items: [],
      error: false,
      action:'LISTAR',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.ListarPaginado, (state) => ({
      ...state,
      loading: true,
      loaded: false,
      items: [],
      error: false,
      action:'LISTAR',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.Listado, (state, { items, pagination }) => ({
      ...state,
      loading: false,
      loaded: true,
      items,
      action:'LISTADO',
      pagination: pagination
    })),
    on(<any>actions.ListarError, (state, { error }) => ({
      ...state,
      loading: false,
      error: error,
      action:'LISTADO_ERROR',
    })),
    on(<any>actions.Obtener, (state, {metadata = null}) => ({
      ...state,
      loading: true,
      loaded: false,
      error: false,
      item: null,
      metadata,
      action: 'OBTENER',
    })),
    on(<any>actions.Obtenido, (state, { item }) => ({
      ...state,
      loading: false,
      loaded: true,
      item,
      action:'OBTENIDO',
    })),
    on(<any>actions.ObtenerError, (state, { error }) => ({
      ...state,
      loading: false,
      error,
      action:'OBTENIDO_ERROR',
    })),
    ...ons
  );
}

export function crudCreateReducer<T>(initialState: any, actions: CrudActions<T>, ...ons: ReducerTypes<any, readonly ActionCreator[]>[]): ActionReducer<any, Action> {
  return createReducer(
    initialState,
    on(<any>actions.Reset, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      items:[],
      item: null,
      metadata: null,
      action:'',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.ResetItem, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      item: null,
      metadata: null,
      action:'',
    })),
    on(<any>actions.ResetItems, (state) => ({
      ...state,
      loading: false,
      error: false,
      loaded:false,
      items:[],
      metadata: null,
      action:'',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.Listar, (state) => ({
      ...state,
      loading: true,
      loaded: false,
      error:false,
      items: [],
      action: 'LISTAR',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.ListarPaginado, (state) => ({
      ...state,
      loading: true,
      loaded: false,
      error:false,
      items: [],
      action: 'LISTAR',
      pagination:{
        page: 1,
        limit: 10,
        total: 0
      }
    })),
    on(<any>actions.Listado, (state, { items, pagination }) => ({
      ...state,
      loading: false,
      loaded: true,
      items,
      action:'LISTADO',
      pagination: pagination
    })),
    on(<any>actions.ListarError, (state, { error }) => ({
      ...state,
      loading: false,
      error,
      action:'LISTADO_ERROR',
    })),
    on(<any>actions.Obtener, (state, { metadata = null }) => ({
      ...state,
      loading: true,
      loaded: false,
      error: false,
      item: null,
      metadata,
      action: 'OBTENER',
    })),
    on(<any>actions.Obtenido, (state, { item }) => ({
      ...state,
      loading: false,
      loaded: true,
      item,
      action:'OBTENIDO',
    })),
    on(<any>actions.ObtenerError, (state, { error }) => ({
      ...state,
      loading: false,
      error,
      action:'OBTENIDO_ERROR',
    })),
    on(<any>actions.Guardar, (state, { item, metadata }) => ({
      ...state,
      loading: true,
      loaded: false,
      error: false,
      item,
      metadata,
      action: 'GUARDAR',
    })),
    on(<any>actions.Guardado, (state, { item }) => ({
      ...state,
      loading: false,
      loaded: true,
      item,
      action:'GUARDADO',
    })),
    on(<any>actions.GuardarError, (state, { error }) => ({
      ...state,
      loading: false,
      loaded: false,
      error,
      action:'GUARDADO_ERROR',
    })),
    on(<any>actions.Actualizar, (state, { item, metadata}) => ({
      ...state,
      loading: true,
      loaded: false,
      error: false,
      item,
      metadata,
      action: 'ACTUALIZAR',
    })),
    on(<any>actions.Actualizado, (state, { item }) => ({
      ...state,
      loading: false,
      loaded: true,
      item,
      action:'ACTUALIZADO',
    })),
    on(<any>actions.ActualizarError, (state, { error }) => ({
      ...state,
      loading: false,
      error,
      action:'ACTUALIZADO_ERROR',
    })),
    on(<any>actions.Eliminar, (state, { item, metadata }) => ({
      ...state,
      loading: true,
      loaded: false,
      error: false,
      item,
      metadata,
      action: 'ELIMINAR',
    })),
    on(<any>actions.Eliminado, (state, { item }) => ({
      ...state,
      loading: false,
      loaded: true,
      item,
      action:'ELIMINADO',
    })),
    on(<any>actions.EliminarError, (state, { error }) => ({
      ...state,
      loading: false,
      error,
      action:'ELIMINADO_ERROR',
    })),
    ...ons
  );
}

export function obtenerReduder<T>(action: (config?: any, metadata?: any) => TypedAction<string>, customAction = 'Obtener'): ReducerTypes<any, any>{
  return on(<any>action, (state, {metadata}) => ({
    ...state,
    loading: true,
    loaded: false,
    error: false,
    item: null,
    metadata,
    action: customAction,
  }));
}

export function listarReduder<T>(action: (config?: any) => TypedAction<string>, customAction = 'Listar'): ReducerTypes<any, any>{
  return on(<any>action, (state) => ({
    ...state,
    loading: true,
    loaded: false,
    items: [],
    error: false,
    action: customAction,
    pagination:{
      page: 1,
      limit: 10,
      total: 0
    }
  }));
}

export function guardarReduder<T>(action: (config?: any) => TypedAction<string>, customAction = 'Guardar'): ReducerTypes<any, any>{
  return on(<any>action, (state, { item, metadata}) => ({
    ...state,
    loading: true,
    loaded: false,
    error: false,
    item,
    metadata,
    action: customAction
  }));
}

export function actualizarReduder<T>(action: (config?: any) => TypedAction<string>, customAction = 'Actualizar'): ReducerTypes<any, any>{
  return on(<any>action, (state, { item, metadata}) => ({
    ...state,
    loading: true,
    loaded: false,
    error: false,
    item,
    metadata,
    action: customAction
  }));
}