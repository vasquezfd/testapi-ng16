import { createFeatureSelector, createSelector } from "@ngrx/store";
import { MAESTROS_CRUD_KEY_FEATURE } from "../admin-app.state";
import { execCreateSelector } from "./basic-selectors";
import { MaestrosCrudAppState, AlumnoMaestroState, PaisState, TipoDocumentoState } from "../maestros-crud.state";

export const MaestrosCrudAppFeature = createFeatureSelector<MaestrosCrudAppState>(MAESTROS_CRUD_KEY_FEATURE);

export const ModalidadMaestroStateFeature = createSelector(
  MaestrosCrudAppFeature,
  (state: MaestrosCrudAppState) => state.alumno
)
export const PaisStateFeature = createSelector(
  MaestrosCrudAppFeature,
  (state: MaestrosCrudAppState) => state.pais
);
export const TipoDocumentoStateFeature = createSelector(
  MaestrosCrudAppFeature,
  (state: MaestrosCrudAppState) => state.tipoDocumento
);

const selectAlumnoMaestro = execCreateSelector<AlumnoMaestroState>(ModalidadMaestroStateFeature);
const selectPais = execCreateSelector<PaisState>(PaisStateFeature);
const selectTipoDocumento = execCreateSelector<TipoDocumentoState>(TipoDocumentoStateFeature);

export const MaestrosCrudConfigSelect = {
  selectAlumnoMaestro,
  selectPais,
  selectTipoDocumento,
}