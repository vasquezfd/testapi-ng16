/* eslint-disable @typescript-eslint/no-explicit-any */


import { HttpErrorResponse } from "@angular/common/http";
import { createSelector} from "@ngrx/store";
import { Pagination } from "../../../core/domain/pagination";
import { GenericStore } from "../basic-state";

export interface ExecSelector {
  select: any,
  obtener: any,
  action: any,
  error: any,
  cargando: any,
  eliminado: any,
  guardado: any,
  actualizado: any,
}

export interface ActionSelector<T extends object>{
  action: string,
  metadata: any,
  error: Error & HttpErrorResponse,
  item: T,
  items: T[],
  pagination: Pagination,
}

export function execCreateSelector<T>(featureSelector: any): ExecSelector
{
  const select = createSelector(
    featureSelector,
    (state: GenericStore<T>) => state.items
  );
  const obtener = createSelector(
    featureSelector,
    (state: GenericStore<T>) => state.item
  );
  const error = createSelector(
    featureSelector,
    (state: GenericStore<T>) => state.error
  );
  const action = createSelector(
    featureSelector,
    (state: GenericStore<T>) => 
    { return {action: state.action, metadata: state.metadata, error: state.error, item: state.item, items: state.items, pagination: state.pagination} }
  );

  const cargando = createSelector(
    featureSelector,
    (state: GenericStore<T>) => state.loading
  );
  
  const eliminado = createSelector(featureSelector, (state: GenericStore<T>) =>
    { return {success: (state.action === 'ELIMINADO' && state.loaded && !state.error), metadata: state.metadata, item: state.item} }
  );

  const guardado = createSelector(featureSelector, (state: GenericStore<T>) =>
    { return {success: (state.action === 'GUARDADO' && state.loaded && !state.error), metadata: state.metadata, item: state.item} }
  );
  
  const actualizado = createSelector(featureSelector, (state: GenericStore<T>) =>
    { return {success: (state.action === 'ACTUALIZADO' && state.loaded && !state.error), metadata: state.metadata, item: state.item} }
  );

  return {
    select,
    obtener,
    action,
    error,
    cargando,
    eliminado,
    guardado,
    actualizado
  }
}
