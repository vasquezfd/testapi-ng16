import { Alumno, Pais, TipoDoc } from "../../core/domain";
import { GenericStore } from "./basic-state";

const INITIAL_STATE = {
  loading: false,
  loaded: false,
  items: [],
  item: null,
  error: false,
  action: '',
}

export interface AlumnoMaestroState extends GenericStore<Alumno>{
  items: Array<Alumno>
}

export interface PaisState extends GenericStore<Pais>{
  items: Array<Pais>
}

export interface TipoDocumentoState extends GenericStore<TipoDoc>{
  items: Array<TipoDoc>
}

export const initialAlumnoMaestroState: AlumnoMaestroState = INITIAL_STATE;
export const initialPaisState: PaisState = INITIAL_STATE;
export const initialTipoDocumentoState: TipoDocumentoState = INITIAL_STATE;

export interface MaestrosCrudAppState {
  alumno: AlumnoMaestroState
  pais: PaisState
  tipoDocumento: TipoDocumentoState
}