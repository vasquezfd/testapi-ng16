import { MaestrosCrudEffects } from "./effects";
import { MaestrosCrudAppState} from "./";
import { MaestrosCrudReducer } from "./reducers";

export const ADMIN_KEY_FEATURE = 'AdminAppState';
export const MAESTROS_CRUD_KEY_FEATURE = 'AdminAppMaestroCrudState';

export interface AdminState {
  maestrosCrud: MaestrosCrudAppState,
}

export const AdminAppState = {
  maestrosCrud: MaestrosCrudReducer,
}

export const ADMIN_EFFECTS: any[] = [
  MaestrosCrudEffects, 
]