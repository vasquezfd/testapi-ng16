import { createAction, props } from "@ngrx/store";
import { TypedAction } from "@ngrx/store/src/models";
import { ResponseGeneric } from "../../../core/domain";

interface ParamsId{
  id: string | number;
  metadata?: any;
}
interface ParamsGeneric{
  [key: string]:string | undefined;
}
interface ParamsItem<T>{
  item: T;
  metadata?: any;
}
interface ParamsItems<T>{
  items: T[];
  metadata?: any;
  pagination?: Pagination;
}
interface ParamsError{
  error: Error;
}

interface Pagination{
  page: number,
  limit: number,
  total: number,
}

type CallbackGeneric = (config?: any) => TypedAction<string>;
type CallbackId = (config: ParamsId) => TypedAction<string>;
type CallbackItem<T> = (creator: any) => ParamsItem<T> & TypedAction<string>;
type CallbackItems<T> = (config: ParamsItems<T>) => ParamsItems<T> & TypedAction<string>;
type CallbackError = (config: ParamsError) => ParamsError & TypedAction<string>;

export interface GenericAction {
  [key: string]: CallbackGeneric
}

export interface ExecActions<T> {
  Reset: CallbackGeneric,
  ResetItem: CallbackGeneric,
  ResetItems: CallbackGeneric,
  Listar: CallbackGeneric,
  ListarPaginado: CallbackGeneric,
  Listado: CallbackItems<T>,
  ListarError: CallbackError,
  Obtener: CallbackId,
  Obtenido: CallbackItem<T>,
  ObtenerError: CallbackError,
}

export interface CrudActions<T> extends ExecActions<T> {
  Guardar: CallbackItem<T>,
  Guardado: CallbackItem<T>,
  GuardarError: CallbackError,
  Actualizar: CallbackItem<T>,
  Actualizado: CallbackItem<T>,
  ActualizarError: CallbackError,
  Eliminar: CallbackItem<T>,
  Eliminado: CallbackItem<T>,
  EliminarError: CallbackError,
}

export function crudCreateActions<T>(recurso: string): CrudActions<T>{
  const Obtener = createAction(
    `[Obtener ${recurso}] Obtener Iniciado`,
    props<{id: string | number;}>()
  );
  const Obtenido = createAction(
    `[Obtenido ${recurso}] Obtener Terminado`,
    props<{item:T; metadata?:any;}>()
  );
  const ObtenerError = createAction(
    `[Obtener Error ${recurso}] Obtener no completado`,
    props<{error: Error}>()
  );
  const Listar = createAction(
    `[Listar ${recurso}] Listar Iniciado`,
    props<{[key: string]: string}>()
  );
  const ListarPaginado = createAction(
    `[Listar Paginado ${recurso}] Listar Paginado Iniciado`,
    props<{[key: string]: string}>()
  );
  const Listado = createAction(
    `[Listado ${recurso}] Listar Terminado`,
    props<{items: T[]; pagination?:Pagination }>()
  );
  const ListarError = createAction(
    `[Listar Error ${recurso}] Listar no completado`,
    props<{error: Error}>()
  );
  const Guardar = createAction(
    `[Guardar ${recurso}] Guardar Iniciado`,
    props<{item:T; metadata?:any;}>()
  );
  const Guardado = createAction(
    `[Guardado ${recurso}] Guardar Terminado`,
    props<{item:T; metadata?:any;}>()
  );
  const GuardarError = createAction(
    `[Guardar Error ${recurso}] Guardar no completado`,
    props<{error: Error}>()
  );
  const Actualizar = createAction(
    `[Actualizar ${recurso}] Actualizar Iniciado`,
    props<{item:T; metadata?:any;}>()
  );
  const Actualizado = createAction(
    `[Actualizado ${recurso}] Actualizar Terminado`,
    props<{item:T; metadata?:any;}>()
  );
  const ActualizarError = createAction(
    `[Actualizar Error ${recurso}] Actualizar no completado`,
    props<{error: Error}>()
  );
  const Eliminar = createAction(
    `[Eliminar ${recurso}] Eliminar Iniciado`,
    props<{item:T; metadata?:any;}>()
  );
  const Eliminado = createAction(
    `[Eliminado ${recurso}] Eliminar Terminado`,
    props<{item:T; metadata?:any;}>()
  );
  const EliminarError = createAction(
    `[Eliminar Error ${recurso}] Listar no completado`,
    props<{error: Error}>()
  );
  const Reset = createAction(
    `[Reset ${recurso}] Estado inicial`,
  );
  const ResetItems = createAction(
    `[Reset Items ${recurso}] Estado inicial`,
  );
  const ResetItem = createAction(
    `[Reset Item ${recurso}] Estado inicial`,
  );

  return {
    Reset,
    ResetItem,
    ResetItems,
    Listar,
    ListarPaginado,
    Listado,
    ListarError,
    Obtener,
    Obtenido,
    ObtenerError,
    Guardar,
    Guardado,
    GuardarError,
    Actualizar,
    Actualizado,
    ActualizarError,
    Eliminar,
    Eliminado,
    EliminarError,
  }
}

export function execCreateActions<T>(recurso: string): ExecActions<T>{
  const Listar = createAction(
    `[Listar ${recurso}] Listar Iniciado`,
    props<{[key: string]: string}>()
  );
  const ListarPaginado = createAction(
    `[Listar Paginado ${recurso}] Listar Paginado Iniciado`,
    props<{[key: string]: string}>()
  );
  const Listado = createAction(
    `[Listado ${recurso}] Listar Terminado`,
    props<{items: T[], pagination?: Pagination }>()
  );
  const ListarError = createAction(
    `[Listar Error ${recurso}] Listar no completado`,
    props<{error: Error}>()
  );
  const Obtener = createAction(
    `[Obtener ${recurso}] Obtener Iniciado`,
    props<{id: string | number}>()
  );
  const Obtenido = createAction(
    `[Obtenido ${recurso}] Obtener Terminado`,
    props<{item:T; metadata?:any;}>()
  );
  const ObtenerError = createAction(
    `[Obtener Error ${recurso}] Obtener no completado`,
    props<{error: Error}>()
  );
  const Reset = createAction(
    `[Reset ${recurso}] Estado inicial`,
  );
  const ResetItems = createAction(
    `[Reset Items ${recurso}] Estado inicial`,
  );
  const ResetItem = createAction(
    `[Reset Item ${recurso}] Estado inicial`,
  );
  
  return {
    Reset,
    ResetItem,
    ResetItems,
    Listar,
    ListarPaginado,
    Listado,
    ListarError,
    Obtener,
    Obtenido,
    ObtenerError,
  }
}

export function execObtenerActions<T>(recurso: string): (config?: any) => TypedAction<string>{
  return createAction(
    `[Obtener ${recurso}] Obtener Iniciado`,
    props<{[key: string]: string}>()
  );
}

export function execListarActions<T>(recurso: string): (config?: any) => TypedAction<string>{
  return createAction(
    `[Listar ${recurso}] Listar Iniciado`,
    props<{[key: string]: string}>()
  );
}

export function execEnviarActions<T>(recurso: string): (config?: any) => TypedAction<string>{
  return createAction(
    `[Actualizar ${recurso}] Actualizar Iniciado`,
    props<{item:T; metadata?:any;}>()
  );
}