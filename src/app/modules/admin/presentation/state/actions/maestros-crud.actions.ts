import { Alumno, Pais, TipoDoc } from '../../../core/domain';
import { CrudActions, ExecActions, crudCreateActions, execCreateActions } from './basic-actions';

export const AlumnoActions: CrudActions<Alumno> = 
  crudCreateActions<Alumno>('Alumno')

export const PaisActions: ExecActions<Pais> = 
  execCreateActions<Pais>('Pais')

export const TipoDocumentoActions: ExecActions<TipoDoc> = 
  execCreateActions<TipoDoc>('Tipo Documento')