export interface GenericStore<T>{
  loading: boolean;
  loaded: boolean;
  items: Array<T>;
  item: T | null;
  error?: Error | boolean;
  action: string;
  metadata?: any;
  pagination?: {
    page: number;
    limit: number;
    total: number;
  }
}