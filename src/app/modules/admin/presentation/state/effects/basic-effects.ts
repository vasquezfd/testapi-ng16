import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of, catchError, map, mergeMap, switchMap } from 'rxjs';
import { CrudActions, ExecActions } from "../actions";
import { TypedAction } from '@ngrx/store/src/models';
import { environment } from 'src/environments/environment';
import { ResponseGeneric } from '../../../core/domain';


type CallbackGeneric<T> = (params: Record<string,string>) => Observable<T[]>;
type CallbackResponseGeneric<T extends Object> = (params: Record<string,string>) => Observable<ResponseGeneric<T>>;
type CallbackItem<T> = (item: T) => Observable<T>;
type CallbackItemMod<P, T> = (item: P) => Observable<T>;
type CallbackId<T> = (id: string | number) => Observable<T>;
type CallbackItemGeneric<T> = (params: Record<string, string>) => Observable<T>;
interface ParamsError{
  error: Error;
}
type CallbackError = (config: ParamsError) => ParamsError & TypedAction<string>;

function CatchEffectError(err: Error, actionError: ParamsError & TypedAction<string>): Observable<ParamsError & TypedAction<string>>{
  if( !environment.production && err !== undefined ){
    if( err.stack !== undefined ){
      console.error(err.stack);
    } else {
      console.error(err.message);
    }
  }
  return of(actionError);
}

export function listarCreateEffect<T>(actions$: Actions, stateActions: ExecActions<T>, operation: CallbackGeneric<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.Listar ),
    switchMap( action => of(action).pipe(
      switchMap((action) => {
        const _action = { ...action };
        delete _action['type'];
        return operation(_action)
          .pipe(
            // delay(500),
            map((items:T[]) => stateActions.Listado({items})),
            catchError(err => CatchEffectError(err, stateActions.ListarError({error: err}) ) )
          )
      }),
      catchError(err => CatchEffectError(err, stateActions.ListarError({error: err})) )
    ))
  ))
}

// export function obtenerItemsEffect<T>(actions$: Actions, stateActions: any, action: string, operation: CallbackItemGeneric<T>){
//   return createEffect(() => actions$.pipe(
//     ofType( stateActions[action] ),
//     switchMap( action => of(action).pipe(
//       switchMap((action) => {
//         const _action = { ...action };
//         delete _action['type'];
//         return operation(_action)
//           .pipe(
//             // delay(500),
//             map((items:T) => stateActions.Obtenido({items})),
//             catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err}) ) )
//           )
//       }),
//       catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err})) )
//     ))
//   ))
// }

export function obtenerEffect<T>(actions$: Actions, stateActions: any, action: string, operation: CallbackItemGeneric<T>){
  return createEffect(() => actions$.pipe(
    ofType( stateActions[action] ),
    switchMap( action => of(action).pipe(
      switchMap((action) => {
        const _action = { ...action };
        delete _action['type'];
        return operation(_action)
          .pipe(
            // delay(500),
            map((item:T) => stateActions.Obtenido({item})),
            catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err}) ) )
          )
      }),
      catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err})) )
    ))
  ))
}

export function listarEffect<T>(actions$: Actions, stateActions: any, action: string, operation: CallbackGeneric<T>){
  return createEffect(() => actions$.pipe(
    ofType( stateActions[action] ),
    switchMap( action => of(action).pipe(
      switchMap((action) => {
        const _action = { ...action };
        delete _action['type'];
        return operation(_action)
          .pipe(
            // delay(500),
            map((items:T[]) => stateActions.Listado({items})),
            catchError(err => CatchEffectError(err, stateActions.ListarError({error: err}) ) )
          )
      }),
      catchError(err => CatchEffectError(err, stateActions.ListarError({error: err})) )
    ))
  ))
}

export function guardarEffect<P, T>(actions$: Actions, stateActions: any, action: string, operation: CallbackItemMod<P, T>){
  return createEffect(() => actions$.pipe(
    ofType( stateActions[action] ),
    mergeMap( action => of(action).pipe(
      mergeMap((action) => operation(action.item)
        .pipe(
          map((item) => stateActions.Guardado({item, metadata: action.metadata}) ),
          catchError(err => CatchEffectError(err, stateActions.GuardarError({error: err}) ) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.GuardarError({error: err})) )
    ))
  ))
}

export function actualizarEffect<P, T>(actions$: Actions, stateActions: any, action: string, operation: CallbackItemMod<P, T>){
  return createEffect(() => actions$.pipe(
    ofType( stateActions[action] ),
    mergeMap( action => of(action).pipe(
      mergeMap((action) => operation(action.item)
        .pipe(
          map((item) => stateActions.Actualizado({item, metadata: action.metadata}) ),
          catchError(err => CatchEffectError(err, stateActions.ActualizarError({error: err}) ) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.ActualizarError({error: err})) )
    ))
  ))
}

export function listarPaginadoCreateEffect<T extends Object>(actions$: Actions, stateActions: ExecActions<T>, operation: CallbackResponseGeneric<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.ListarPaginado ),
    switchMap( action => of(action).pipe(
      switchMap((action) => {
        const _action = { ...action };
        delete _action['type'];
        return operation(_action)
          .pipe(
            // delay(500),
            map((items:ResponseGeneric<T>) => stateActions.Listado(
              {
                items: items.rows,
                pagination: {
                  page: items.page,
                  limit: items.limit,
                  total: items.total
                }
              }
            )),
            catchError(err => CatchEffectError(err, stateActions.ListarError({error: err}) ) )
          )
      }),
      catchError(err => CatchEffectError(err, stateActions.ListarError({error: err})) )
    ))
  ))
}

export function obtenerCreateEffect<T>(actions$: Actions, stateActions: CrudActions<T> | ExecActions<T>, operation: CallbackId<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.Obtener ),
    switchMap( action => of(action).pipe(
      switchMap((action) => operation(action.id)
        .pipe(
          map((item) => stateActions.Obtenido({item})),
          catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err})) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.ObtenerError({error: err})) )
    ))
  ))
}

export function guardarCreateEffect<T>(actions$: Actions, stateActions: CrudActions<T>, operation: CallbackItem<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.Guardar ),
    mergeMap( action => of(action).pipe(
      mergeMap( action => operation(action.item)
        .pipe(
          map((item) => stateActions.Guardado({item, metadata: action.metadata}) ),
          catchError(err => CatchEffectError(err, stateActions.GuardarError({error: err}) ) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.GuardarError({error: err})) )
    ))
  ))
}

export function actualizarCreateEffect<T>(actions$: Actions, stateActions: CrudActions<T>, operation: CallbackItem<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.Actualizar ),
    mergeMap( action => of(action).pipe(
      mergeMap((action) => operation(action.item)
        .pipe(
          map((item) => {
            return stateActions.Actualizado({item: item !== null ? item : action.item, metadata: action.metadata})
          }),
          catchError(err => CatchEffectError(err, stateActions.ActualizarError({error: err})) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.ActualizarError({error: err})) )
    ))
  ))
}

export function eliminarCreateEffect<T>(actions$: Actions, stateActions: CrudActions<T>, operation: CallbackItem<T>){
  return createEffect(() => actions$.pipe(
    ofType( <any>stateActions.Eliminar ),
    mergeMap( action => of(action).pipe(
      mergeMap((action) => operation(action.item)
        .pipe(
          map((item) => stateActions.Eliminado({item: item !== null ? item : action.item, metadata: action.metadata})),
          catchError(err => CatchEffectError(err, stateActions.EliminarError({error: err})) )
        )
      ),
      catchError(err => CatchEffectError(err, stateActions.EliminarError({error: err})) )
    ))
  ))
}

// export function resetCreateEffect<T>(actions$: Actions, stateActions: ExecActions<T>){
//   return createEffect(() => actions$.pipe(
//     ofType( <any>stateActions.Reset ),
//     switchMap( action => of(action).pipe(
//       switchMap((action) => {
//         const _action = { ...action };
//         delete _action['type'];
//         return of(_action)
//       }),
//       catchError(err => of(err) )
//     ))
//   ))
// }

// export function errorCreateEffect<T>(actions$: Actions, stateActions: CrudActions<T>): Observable<Error>{
//   return createEffect(() => actions$.pipe(
//     ofType( <any>stateActions.ActualizarError, <any>stateActions.EliminarError, <any>stateActions.GuardarError, <any>stateActions.ListarError, <any>stateActions.ObtenerError ),
//     switchMap((action) => {
//       const _action = { ...action };
//       delete _action['type'];
//       return of(_action)
//     })
//   ))
// }