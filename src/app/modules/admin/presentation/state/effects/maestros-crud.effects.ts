import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { CrudAlumno, ListarPais } from '../../../core/usecases';
import { AlumnoActions, PaisActions, TipoDocumentoActions } from '../actions';
import { actualizarCreateEffect, eliminarCreateEffect, guardarCreateEffect, listarCreateEffect, obtenerCreateEffect } from './basic-effects';

@Injectable({
  providedIn: 'root'
})
export class MaestrosCrudEffects {
  
  constructor(
    private actions$: Actions,
    private repoAlumno: CrudAlumno,
    private repoPais: ListarPais,
    ) { }
 
  //Alumno
  listarAlumno$ =
    listarCreateEffect(this.actions$, AlumnoActions, this.repoAlumno.listar.bind(this.repoAlumno))
  
  obtenerAlumno$ =
    obtenerCreateEffect(this.actions$, AlumnoActions, this.repoAlumno.obtener.bind(this.repoAlumno))
  
  guardarAlumno$ =
    guardarCreateEffect(this.actions$, AlumnoActions, this.repoAlumno.guardar.bind(this.repoAlumno))
  
  actualizarAlumno$ =
    actualizarCreateEffect(this.actions$, AlumnoActions, this.repoAlumno.actualizar.bind(this.repoAlumno))
  
  eliminarAlumno$ =
    eliminarCreateEffect(this.actions$, AlumnoActions, this.repoAlumno.eliminar.bind(this.repoAlumno))
 
  
  obtenerTipoDocumento$ =
    listarCreateEffect(this.actions$, TipoDocumentoActions, this.repoAlumno.listarTipoDocumento.bind(this.repoAlumno))

  //Pais
  listarPais$ =
    listarCreateEffect(this.actions$, PaisActions, this.repoPais.execute.bind(this.repoPais))
}
