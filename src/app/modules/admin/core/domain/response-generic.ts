export interface ResponseGeneric<T extends Object>{
  page: number,
  pagina?: number,
  limit: number,
  porPagina?: number,
  total: number,
  rows: Array<T>,
  datos?: Array<T>,
}