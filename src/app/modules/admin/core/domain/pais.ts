export interface Pais {
    name: {
        common:string, 
        official: string, 
        nativeName?: any
    },
    tld?: string[]
    cca2: string
    ccn3?: string
    cca3: string
    cioc?: string
    independent?: boolean
    status: string
    unMember: boolean
    currencies?: any,
    idd: {
        root: string
        suffixes: string[]
    }
    capital?: string[]
    altSpellings?: string[]
    region: string
    subregion?: string
    languages?: any
    translations?: any
    latlng: number[]
    landlocked: boolean
    borders?: string[]
    area: number
    demonyms?: {[key:string]:{
        f:string,
        m:string
    }}
    flag: string
    maps: {
        googleMaps: string
        openStreetMaps: string
    }
    population: number
    gini?: any
    fifa?: string
    car: {
        signs: string[]
        side: string
    }
    timezones: string[]
    continents: string[]
    flags: {
        png: string
        svg: string
        alt: string
    }
    coatOfArms?: {
        png?: string
        svg?: string
    }
    startOfWeek: string
    capitalInfo: {
        latlng: number[]
    }
    postalCode?: {
        format: string
        regex: string
    }
  }