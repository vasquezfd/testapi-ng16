export * from './alumno';
export * from './pagination';
export * from './pais';
export * from './response-blob';
export * from './response-generic';
export * from './tipo-doc';
