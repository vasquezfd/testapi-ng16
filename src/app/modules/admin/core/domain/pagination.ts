export interface Pagination {
  page: number,
  limit: number,
  total: number,
  order: string,
  orderType: string,
}

export const PAGINATION_INIT = {
  page: 1,
  limit: 10,
  total: 0,
  order: '',
  orderType: 'desc'
}

export interface PaginationAlter<T> {
  data: T[],
  paginaActual: number,
  porPagina: number,
  total: number
}