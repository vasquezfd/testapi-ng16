import { TipoDoc } from "./"

export interface Alumno {
    idAlumno: number
    nombres: string
    apellidos: string
    email: string
    idTipoDocumento: number
    numeroDocumento: string
    fecNacimiento: Date
    sexo: string
    telefono: string
    nacionalidad: string
    createdAt: string
    updatedAt: string
    objectId: string
    tipoDocumento?: TipoDoc
}
  