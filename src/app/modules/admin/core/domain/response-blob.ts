export interface ResponseBlob {
  blob: Blob | null,
  name: string, 
  error: string | null,
}