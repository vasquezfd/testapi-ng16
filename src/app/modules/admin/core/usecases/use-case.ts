import { Observable } from 'rxjs';

export interface UseCase<S, T> {
  execute(params: S): Observable<T>;
}

export interface UseCase2<S, K, T> {
  execute(param: S, param2: K): Observable<T>;
}

export interface UseCaseCrud<S, T> {
  obtener(id: string | number): Observable<T>;
  listar(params: { [key: string]: string } | null): Observable<T[]>;
  guardar(params: S): Observable<T>;
  actualizar(params: S): Observable<T>;
  eliminar(params: S): Observable<T>;
}

export type ParamsGeneric = { [key: string]: string } | null;
