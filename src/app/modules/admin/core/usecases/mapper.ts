import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { catchError, first, mergeMap, Observable, of, switchMap } from "rxjs";
import { ResponseBlob } from "../domain";
import { ResponseGeneric } from "../domain/response-generic";

export abstract class Mapper<I extends Object, O extends Object> {
  abstract mapFrom(param: I): O;
  abstract mapTo(param: O): I;
  
  pipeArrayMapFrom(data: Observable<I[]>) : Observable<O[]>{
    return data.pipe(
      first(),
      mergeMap(data => of(
        data.map(row => this.mapFrom(row))
      ))
    )
  }
  
  pipeArrayMapTo(data: Observable<O[]>) : Observable<I[]>{
    return data.pipe(
      first(),
      mergeMap(data => of(
        data.map(row => this.mapTo(row))
      ))
    )
  }

  pipeResponseArrayMapFrom(data: Observable<ResponseGeneric<I>>) : Observable<ResponseGeneric<O>>{
    return data.pipe(
      first(),
      mergeMap(data => {
        let rows: O[] = []
        if( data.datos !== undefined ){
          rows = data.datos.map(row => this.mapFrom(row))
        } else if( data.rows !== undefined){
          rows = data.rows.map(row => this.mapFrom(row))
        }

        return of({
          page: (data.pagina ?? data.page) - 1,
          pagina: (data.pagina ?? data.page) - 1,
          limit: (data.porPagina ?? data.limit),
          porPagina: (data.porPagina ?? data.limit),
          total: (data.total),
          rows,
          datos:rows
        })
      })
    )
  }

  pipeItemMapFrom(data: Observable<I>, def?: any) : Observable<O>{
    return data.pipe(
      first(),
      mergeMap(data => of(
        data !== null && def === undefined
          ? this.mapFrom(data)
          : def
      )),
      catchError( (err: HttpErrorResponse) => {
        throw err
      }),
    )
  }

  pipeResponseBlob(data: Observable<HttpResponse<Blob>>) : Observable<ResponseBlob>{
    return data.pipe(
      first(),
      mergeMap(data => {
        return of(
          data.status === 200
            ? { blob: data.body, name: '', error: null } 
            : { blob: null, name: '', error: null } 
        )
      }),
      catchError( (err: HttpErrorResponse) => {
        // throw err
        return of({ blob: null, name: '', error: err.message ?? '' })
      }),
    )
  }
  
  lowerCaseKeys = (obj: any): any => {
    if (typeof obj !== 'object') {
        return obj;
    }
    if (Array.isArray(obj)) {
        return obj.map(this.lowerCaseKeys);
    }
    if (obj === null) {
        return null;
    }
    const entries = Object.entries(obj);
    const mappedEntries = entries.map(
      ([k, v]) => [`${k.substring(0, 1).toLowerCase()}${k.substring(1)}`, this.lowerCaseKeys(v)] as const
    );
    return Object.fromEntries(mappedEntries);
  };
  
  upperCaseKeys = (obj: any): any => {
    if (typeof obj !== 'object') {
        return obj;
    }
    if (Array.isArray(obj)) {
        return obj.map(this.upperCaseKeys);
    }
    if (obj === null) {
        return null;
    }
    const entries = Object.entries(obj);
    const mappedEntries = entries.map(
      ([k, v]) => [`${k.substring(0, 1).toUpperCase()}${k.substring(1)}`, this.upperCaseKeys(v)] as const
    );
    return Object.fromEntries(mappedEntries);
  };
}