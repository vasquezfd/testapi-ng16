import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParamsGeneric, UseCaseCrud } from './';
import { Alumno, TipoDoc } from '../domain';
import { AlumnoRepository } from '../repositories';

type T = Alumno;

@Injectable({
  providedIn: 'root'
})
export class CrudAlumno implements UseCaseCrud<Alumno,Alumno> {

  constructor(private alumnoRepository: AlumnoRepository) { }
  obtener(id: string | number): Observable<T> {
    return this.alumnoRepository.obtenerAlumno(id);
  }
  listar(params: ParamsGeneric = null): Observable<T[]> {
    return this.alumnoRepository.listarAlumno(params);
  }
  guardar(params: T): Observable<T> {
    return this.alumnoRepository.guardarAlumno(params);
  }
  actualizar(params: T): Observable<T> {
    return this.alumnoRepository.actualizarAlumno(params);
  }
  eliminar(params: T): Observable<T> {
    return this.alumnoRepository.eliminarAlumno(params);
  }

  listarTipoDocumento(params: ParamsGeneric = null): Observable<TipoDoc[]>{
    return this.alumnoRepository.tipoDocumento(params)
  }
}
