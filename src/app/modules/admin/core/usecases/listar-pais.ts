import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParamsGeneric, UseCase } from '.';
import { Pais } from '../domain';
import { PaisRepository } from '../repositories';

type T = Pais

@Injectable({
  providedIn: 'root'
})
export class ListarPais implements UseCase<ParamsGeneric,T[]> {

  constructor(private paisRepository: PaisRepository) { }
  execute(params: ParamsGeneric = null): Observable<T[]> {
    return this.paisRepository.listaPais(params);
  }
}
