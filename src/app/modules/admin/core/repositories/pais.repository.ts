import { Observable } from 'rxjs';
import { Pais } from '../domain';
import { ParamsGeneric } from '../usecases';

type T = Pais;

export abstract class PaisRepository {
  abstract listaPais(params: ParamsGeneric): Observable<T[]>;
}
