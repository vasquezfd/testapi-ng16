import { Injectable } from '@angular/core';
import { CrudAlumno, ListarPais } from '../usecases';
import { AlumnoRepository, PaisRepository } from './';
import { AlumnoFakeRepository, PaisFakeRepository } from '../../data/repository/fake';
import { AlumnoHttpRepository, PaisHttpRepository } from '../../data/repository/http';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor() { }

  static providers(type: string){
    let providers: any[] =[
        CrudAlumno,
        ListarPais,
    ];

    switch(type){
      case 'fake':
        providers = providers.concat([
          { provide: AlumnoRepository, useClass: AlumnoFakeRepository },
          { provide: PaisRepository, useClass: PaisFakeRepository },
        ])
        break;
      case 'http':
        providers = providers.concat([
          { provide: AlumnoRepository, useClass: AlumnoHttpRepository },
          { provide: PaisRepository, useClass: PaisHttpRepository },
        ])
        break;
    }
    return providers;
  }
}
