import { Observable } from 'rxjs';
import { Alumno, TipoDoc } from '../domain';
import { ParamsGeneric } from '../usecases';

type T = Alumno;

export abstract class AlumnoRepository {
  abstract obtenerAlumno(id: string | number): Observable<T>;
  abstract listarAlumno(params: ParamsGeneric): Observable<T[]>;
  abstract guardarAlumno(alumno: T): Observable<T>;
  abstract actualizarAlumno(alumno: T): Observable<T>;
  abstract eliminarAlumno(alumno: T): Observable<T>;

  abstract tipoDocumento(params: ParamsGeneric): Observable<TipoDoc[]>;
}
