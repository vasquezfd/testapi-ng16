import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pais } from '../../../core/domain';
import { PaisRepository } from '../../../core/repositories';
import { ParamsGeneric } from '../../../core/usecases';
import { HttpService, PaisDto, PaisHttpMapper } from '.';


type T = Pais;
type Dto = PaisDto;

@Injectable({
  providedIn: 'root'
})
export class PaisHttpRepository implements PaisRepository {
    mapper = new PaisHttpMapper();
    http: HttpClient;
    uri:string;

    constructor(private h: HttpService){
      this.uri = this.h.apiNaciones;
      this.http = this.h.http
    }

    listaPais(params: ParamsGeneric): Observable<Pais[]> {
      const obs$ = this.http.get<Dto[]>(
        `${this.uri}/all`
      )
      return this.mapper.pipeArrayMapFrom(obs$)
    }
}
