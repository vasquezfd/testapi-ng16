import { TipoDocDto } from "./tipo-doc-dto"

export interface AlumnoDto {
    idAlumno: number|string
    nombres: string
    apellidos: string
    email: string
    idTipoDocumento: number
    numeroDocumento: string
    fecNacimiento: string
    sexo: boolean
    telefono: string
    nacionalidad: string
    createdAt: string
    updatedAt: string
    objectId: string
    tipoDocumento?: TipoDocDto
}
  