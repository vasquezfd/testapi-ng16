import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Alumno, TipoDoc } from '../../../core/domain';
import { AlumnoRepository } from '../../../core/repositories';
import { ParamsGeneric } from '../../../core/usecases';
import { HttpService, AlumnoDto, AlumnoHttpMapper, TipoDocHttpMapper } from '.';
import { TipoDocDto } from './dto/tipo-doc-dto';


type T = Alumno;
type Dto = AlumnoDto;

@Injectable({
  providedIn: 'root'
})
export class AlumnoHttpRepository implements AlumnoRepository {
    mapper = new AlumnoHttpMapper();
    mapperTipoDoc = new TipoDocHttpMapper();
    http: HttpClient;
    uri:string;
    uriTipoDoc:string;

    constructor(private h: HttpService){
      this.uri = 'Alumno';
      this.uriTipoDoc = 'TipoDocumento';
      this.http = this.h.http
    }

    obtenerAlumno(id: string | number): Observable<T> {
      const obs$ = this.http.get<Dto>(
        this.h.base(`${this.uri}/Details/${id.toString()}`)
      )
      return this.mapper.pipeItemMapFrom(obs$)
    }
    listarAlumno(params: ParamsGeneric = null): Observable<T[]> {
      const obs$ = this.http.get<Dto[]>(
        this.h.base(`${this.uri}/Index`)
      )
      return this.mapper.pipeArrayMapFrom(obs$);
    }
    guardarAlumno(alumno: T): Observable<T> {
      const _alumno = <any>this.mapper.mapTo(alumno)
      delete _alumno.idAlumno
      delete _alumno.tipoDocumento

      const obs$ = this.http.post<Dto>(
        this.h.base(`${this.uri}/Create`),
        _alumno
      )
      return this.mapper.pipeItemMapFrom(obs$)
    }
    actualizarAlumno(alumno: T): Observable<T> {
      const _alumno = <any>this.mapper.mapTo(alumno)
      delete _alumno.tipoDocumento

      const obs$ = this.http.post<Dto>(
        this.h.base(`${this.uri}/Update/${alumno.idAlumno.toString()}`),
        _alumno
      )
      return this.mapper.pipeItemMapFrom(obs$, alumno)
    }
    eliminarAlumno(alumno: T): Observable<T> {
      const obs$ = this.http.delete<Dto>(
        this.h.base(`${this.uri}/Delete/${alumno.idAlumno.toString()}`)
      )
      return this.mapper.pipeItemMapFrom(obs$)
    }

    tipoDocumento(): Observable<TipoDoc[]> {
      const obs$ = this.http.get<TipoDocDto[]>(
        this.h.base(`${this.uriTipoDoc}/Index`)
      )
      return this.mapperTipoDoc.pipeArrayMapFrom(obs$)
    }
}
