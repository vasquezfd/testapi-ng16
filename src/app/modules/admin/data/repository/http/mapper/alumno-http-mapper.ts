import { Mapper } from "../../../../core/usecases/mapper";
import { Alumno, TipoDoc } from "../../../../core/domain";
import { AlumnoDto, TipoDocDto } from "../dto";
import { FORMAT_DATE_ALT, JSDateToString, stringToJSDate } from "src/app/modules/admin/services/utils";
import { TipoDocHttpMapper } from "./tipo-doc-http-mapper";

export class AlumnoHttpMapper extends Mapper <AlumnoDto, Alumno> {
    mapperTipoDoc: TipoDocHttpMapper = new TipoDocHttpMapper
    mapFrom(param: AlumnoDto): Alumno {
        return {
            ... this.lowerCaseKeys(param),
            idAlumno: <number><unknown>param?.idAlumno ?? 0,
            sexo: param.sexo === true
                ? 'MASCULINO'
                : 'FEMENINO',
            fecNacimiento: stringToJSDate(param.fecNacimiento, FORMAT_DATE_ALT),
            tipoDocumento: this.mapperTipoDoc.mapFrom(<TipoDocDto>param.tipoDocumento)
        }
    }

    mapTo(param: Alumno): AlumnoDto {
        return {
            // ... this.upperCaseKeys(param),
            ...param,
            idAlumno: param.idAlumno,
            sexo: (param.sexo === 'MASCULINO'),
            fecNacimiento: typeof param.fecNacimiento === 'object'
                ? <string>JSDateToString(param.fecNacimiento, FORMAT_DATE_ALT)
                : '',
            tipoDocumento: this.mapperTipoDoc.mapTo(<TipoDoc>param.tipoDocumento)
        }
    }
  
}
