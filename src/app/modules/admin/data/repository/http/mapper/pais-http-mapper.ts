import { Mapper } from "../../../../core/usecases/mapper";
import { Pais } from "../../../../core/domain";
import { PaisDto } from "../dto";

export class PaisHttpMapper extends Mapper <PaisDto, Pais> {
    mapFrom(param: PaisDto): Pais {
        return {
            ... this.lowerCaseKeys(param),
        }
    }

    mapTo(param: Pais): PaisDto {
        return {
            ... this.upperCaseKeys(param),
        }
    }
  
}
