import { Mapper } from "../../../../core/usecases/mapper";
import { TipoDoc } from "../../../../core/domain";
import { TipoDocDto } from "../dto";

export class TipoDocHttpMapper extends Mapper <TipoDocDto, TipoDoc> {
    mapFrom(param: TipoDocDto): TipoDoc {
        return {
            tipoDocumento: (param?.tipoDoc ?? '').toUpperCase(),
            idTipoDocumento: param?.idTipoDoc
        }
    }

    mapTo(param: TipoDoc): TipoDocDto {
        return {
            tipoDoc: param?.tipoDocumento,
            idTipoDoc: param?.idTipoDocumento
        }
    }
  
}
