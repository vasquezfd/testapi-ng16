import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, switchMap } from 'rxjs';
import { environment } from '../../../../../../environments/environment'
import { ParamsGeneric } from '../../../core/usecases';

@Injectable({
  providedIn: 'root'
})

export class HttpService {
  private _apiUrl: string
  private _apiNaciones: string
  
  get apiUrl(): string {
    return this._apiUrl;
  }

  get apiNaciones(): string {
    return this._apiNaciones;
  }

  get http(): HttpClient {
    return this._http;
  }

  constructor(private _http: HttpClient) {
    this._apiUrl = `${environment.apiUrl}/`
    this._apiNaciones = `${environment.apiNaciones}/`
  }

  base(path: string = ''){
    let uri = `${this._apiUrl}${path}`;
    if (uri[uri.length-1] === '/'){
      uri = uri.substring(0, uri.length-1)
    }
    return uri
  }

  getParam(params: ParamsGeneric, key: string, def: any): any{
    let param = def
    if( params !== null && params[key] !== undefined ){
      param = params[key] ?? def
    }
    return param;
  }

  proccessCode(obs$: Observable<any>, code: number): Observable<boolean>{
    return obs$.pipe(
      switchMap((response: HttpResponse<{}>) => {
        return of(response.status === code)
      }),
      catchError((err: Error) => {
        console.log(err);
        throw err;
      })
    );
  }

}
