import { Observable, map, of } from 'rxjs';
import { FakeTableGeneric } from '.';
import { Pais } from '../../../core/domain';
import { PaisRepository } from '../../../core/repositories';
import { ParamsGeneric } from '../../../core/usecases';
import { PaisHttpMapper } from '../http';

import respondPais from './mock/Pais.json';
import _ from 'lodash';
type T = Pais;

export class PaisFakeRepository implements PaisRepository {
  
  mapper = new PaisHttpMapper();
  crud: FakeTableGeneric<T> = new FakeTableGeneric(respondPais, 'idPais', this.mapper);

  listaPais(params: ParamsGeneric): Observable<Pais[]> {
    return of( _.cloneDeep( respondPais ) ).pipe( 
      map( (data) => data.map(item => this.mapper.mapFrom(<any>item)))
    );
  }
  
}
