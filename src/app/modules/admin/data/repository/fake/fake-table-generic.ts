/* eslint-disable @typescript-eslint/no-explicit-any */
import _ from 'lodash';
import { Observable, of, throwError } from 'rxjs';

export class FakeTableGeneric<T>{
  id:string;
  table:any[]
  mapper:any;
  upperCaseAttrib = false

  constructor(table:any[], id:string, mapper:any){
    this.table = table;
    this.id = id;
    this.mapper = mapper;
  }
  
  obtener<P extends Object>(id: string | number, idColumn?: string, table?: any[]): Observable<T> | Observable<P> {
    const _table = _.cloneDeep(table ?? this.table )
    const _idColumn = idColumn ?? this.id;
    // const row = _.find(_table, [_idColumn, id]);
    const params: {[key :string]: string} = {}
    params[_idColumn] = <string>id
    const rows = this.filter(_table, params );
    if (rows.length > 0){
      return of( this.mapper.mapFrom(rows[0]) );
    } else {
      throw throwError(() => new Error(`No se Encontró el Objeto`));
    }
  }
  listar(params: {[key :string]: string} | null = null, table?: any[]): Observable<T[]> {
    const _table = _.cloneDeep(table ?? this.table )

    if( params !== null ){
      return of( 
        this.filter(_table, params)
          .map(row => this.mapper.mapFrom(row))
      )
    } else {
      return of([])
    }
  }
  guardar(row: T): Observable<T> {
    const _row = this.mapper.mapTo(row)
    if( _row[this.id] === null || _row[this.id] === undefined || _row[this.id] === '' ){
      const maxRow = _.maxBy(this.table, this.id);
      if (maxRow !== undefined && maxRow[this.id] !== undefined){
        _row[this.id] = <string><unknown>( Number( maxRow[this.id] ) + 1);
      } else {
        _row[this.id] = 1;
      }
    }
    this.table.push( _row )
    return of ( this.mapper.mapFrom( _row ) );
  }
  actualizar(row: T): Observable<T> {
    const _row = this.mapper.mapTo(row)
    // let item = _.find(this.table, [this.id, (_row)[this.id]])
    const params: {[key :string]: string} = {}
    params[this.id] = <string>(_row)[this.id]
    const rows = this.filter(this.table, params);
    
    if( rows.length > 0 ){
      let item = rows[0];
      item = _.assign(item, _row);
      return of ( this.mapper.mapFrom(item) )
    } else {
      throw throwError(() => new Error(`No se Encontró el Objeto`));
    }
  }
  eliminar(row: T): Observable<T> {
    const _row = this.mapper.mapTo(row)
    // const item = _.find(this.table, [this.id, _row[this.id]])
    const params: {[key :string]: string} = {}
    params[this.id] = <string>(_row)[this.id]
    const rows = this.filter(this.table, params);

    if( rows.length > 0 ){
      let item = rows[0]
      _.pull(this.table, item)
      return of ( this.mapper.mapFrom(item) );
    } else {
      throw throwError(() => new Error(`No se Encontró el Objeto`));
    }
  }
  filter(table: any[], params: {[key :string]: string}): any[]{
    return _.filter(table, (item: any) => {
      let ret = true;
      _.mapKeys(this.upperCaseAttrib ? this.mapper.upperCaseKeys(params) : params, function(val: any, key: string) {
        if( val === undefined){
          ret = false
        } else {
          ret = ret && (item[key])?.toString() === (val !== null ? val.toString() : val)
        }
      });
      return ret;
    })
  }
  update(paramsId: {[key :string]: any}, value: {[key :string]: any}){
    const rows = this.filter(this.table, paramsId)
    if( rows.length > 0 ){
      const item = rows[0]
      _.assign(item, this.upperCaseAttrib ? this.mapper.upperCaseKeys(value) : value);
      return true
    } else {
      return false;
    }
  }
  obtenerUltimo(): Observable<T> {
    const maxRow = _.maxBy(this.table, this.id);
    return of ( this.mapper.mapFrom( maxRow ) );
  }
}
