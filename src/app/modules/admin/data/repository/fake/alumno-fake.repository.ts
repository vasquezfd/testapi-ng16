import { Observable } from 'rxjs';
import { FakeTableGeneric } from '.';
import { Alumno, TipoDoc } from '../../../core/domain';
import { AlumnoRepository } from '../../../core/repositories';
import { ParamsGeneric } from '../../../core/usecases';
import { AlumnoHttpMapper, TipoDocHttpMapper } from '../http';

import respondAlumno from './mock/Alumno.json';
import respondTipoDoc from './mock/TipoDocumento.json';
type T = Alumno;

export class AlumnoFakeRepository implements AlumnoRepository {
  mapper = new AlumnoHttpMapper();
  mapperTipoDoc = new TipoDocHttpMapper();
  crud: FakeTableGeneric<T> = new FakeTableGeneric(respondAlumno, 'idAlumno', this.mapper);
  crudTipoDoc: FakeTableGeneric<TipoDoc> = new FakeTableGeneric(respondTipoDoc, 'idTipoDocumento', this.mapperTipoDoc);

  obtenerAlumno(id: string | number): Observable<T> {
    return this.crud.obtener(id);
  }
  listarAlumno(params: ParamsGeneric = null): Observable<T[]> {
    return this.crud.listar(params)
  }
  guardarAlumno(Alumno: T): Observable<T> {
    return this.crud.guardar(Alumno);
  }
  actualizarAlumno(Alumno: T): Observable<T> {
    return this.crud.actualizar(Alumno);
  }
  eliminarAlumno(Alumno: T): Observable<T> {
    return this.crud.eliminar(Alumno);
  }

  tipoDocumento(params: ParamsGeneric = null): Observable<TipoDoc[]> {
    return this.crudTipoDoc.listar(params)
  }
}
