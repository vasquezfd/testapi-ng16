// import { FormGroug } from "@angular/forms";
import _ from "lodash";
import { DateTime } from "luxon";
import { first, Observable } from "rxjs";
// import { EncryptDecryptService } from "src/app/core/services";
// import { InputsError } from "src/app/shared/components/table-edit/table-edit.component";

export const DATE_ZONE = 'America/Lima';
export const DATE_LOCALE = 'es-PE';
export const FORMAT_DATE = 'dd/MM/yyyy';
export const FORMAT_DATE_ALT = 'yyyy-MM-dd';
export const FORMAT_TIME = 'hh:mm:ss';
export const FORMAT_TIME_SHORT = 'hh:mm';
export const FORMAT_DATETIME = `${FORMAT_DATE} ${FORMAT_TIME}`;

export function stringToJSDate(date: string | null, format = FORMAT_DATE): Date | null{
  if( date === null){
    return null;
  } 
  let _date = DateTime.fromFormat(date, format);
  if( !_date.isValid ){
    _date = DateTime.fromFormat(date.substring(0, format.length), format);
  }
  return _date.isValid ? _date.toJSDate() : null;
}

export function JSDateToString(date: Date | null, format = FORMAT_DATE): string | null {
  if( date === null){
    return null;
  } 
  const _date = DateTime.fromJSDate(date);
  return _date.isValid ? _date.toFormat(format) : null;
}

export function minDate(a: Date, b: Date): Date{
  return a < b ? a : b
}

export function maxDate(a: Date, b: Date): Date{
  return a > b ? a : b
}

export function toBoolean( data: any, def = false ): boolean{
  let ret: boolean;

  switch( typeof data ){
    case 'string':
      ret = (data.toLowerCase() === 'true');
      break;
    case 'boolean':
      ret = data;
      break;
    case 'undefined':
    default:
      ret = def;
  }
  return ret
}

export const lowerCaseKeys = (obj: any): any => {
  if (obj === undefined){
    return {};
  }
  if (typeof obj !== 'object') {
      return obj;
  }
  if (Array.isArray(obj)) {
      return obj.map(lowerCaseKeys);
  }
  if (obj === null) {
      return null;
  }

  const entries = Object.entries(obj);
  const mappedEntries = entries.map(
    ([k, v]) => [`${k.substring(0, 1).toLowerCase()}${k.substring(1)}`, lowerCaseKeys(v)] as const
  );
  return Object.fromEntries(mappedEntries);
};

export const upperCaseKeys = (obj: any): any => {
  if (obj === undefined){
    return {};
  }
  if (typeof obj !== 'object') {
      return obj;
  }
  if (Array.isArray(obj)) {
      return obj.map(upperCaseKeys);
  }
  if (obj === null) {
      return null;
  }
  const entries = Object.entries(obj);
  const mappedEntries = entries.map(
    ([k, v]) => [`${k.substring(0, 1).toUpperCase()}${k.substring(1)}`, upperCaseKeys(v)] as const
  );
  return Object.fromEntries(mappedEntries);
};

export const escapeObject = (obj: any): any => {
  if (obj === undefined){
    return {};
  }
  if (typeof obj !== 'object') {
      return obj;
  }
  if (Array.isArray(obj)) {
      return obj.map(escapeObject);
  }
  if (obj === null) {
      return null;
  }

  const entries = Object.entries(obj);
  const mappedEntries = entries.map(
    ([k, v]) => [k, _.escape(<string>v)] as const
  );
  return Object.fromEntries(mappedEntries);
};

// export const toInputsError = (validationError: {[key:string]:string[]}): InputsError => {
//   let errors: InputsError = {}
//   const _validationError = lowerCaseKeys(validationError);
//   if( ![undefined, null].includes(_validationError) ){
//     Object.keys(_validationError).forEach(key => {
//       errors[key] = {
//         server: _validationError[key].join('<br>'),
//       }
//     })
//   }
//   return errors
// }

// export const setFormGroupErrors = (form: FormGroup, validationError: {[key:string]:string[]}) => {
//   const errors = toInputsError(validationError);

//   Object.keys(errors).forEach(key => {
//     const formControl = form.get(key);
//     if (formControl) {
//       formControl.setErrors(errors[key]);
//     }
//   });
// }


export const latLngValid = (latitud: number | string, longitud: number | string) => {
  return (
    !isNaN(parseFloat(<string>latitud)) && 
    <string>latitud !== '0' && 
    !isNaN(parseFloat(<string>longitud)) && 
    <string>longitud !== '0'
  )
}

export const downloadBlob = (blob: Blob, name = 'file.txt') => {
  const blobUrl = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = blobUrl;
  link.download = name;
  document.body.appendChild(link);
  link.dispatchEvent(
    new MouseEvent('click', { 
      bubbles: true, 
      cancelable: true, 
      view: window 
    })
  );
  document.body.removeChild(link);
}

export const obsToValue = <T>(obs$: Observable<T>): T | null => {
  let item = null
  obs$
    .pipe( first() )
    .subscribe( (_item: T) => {
      item = _item
    })
  return item
}

export const obsToArray = <T>(obs$: Observable<T>): T | [] => {
  const items = obsToValue(obs$)
  return items !== null ? items : []
}

export const compareString = (str1: string, str2: string) => {
  let s1 = str1.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  let s2 = str2.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

  return s1.toLowerCase().indexOf(s2.toLowerCase()) > -1;
}