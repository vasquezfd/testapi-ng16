import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from  './modules/admin/admin.component';
import { GuestComponent } from './modules/guest/guest.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'admin/alumno/lista',
    pathMatch: 'full',
  },
  {
    path: 'admin',
    component: AdminComponent,
    // canActivate: [AuthGuardService],
    children: [{
      path: '',
      loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
    }]
  },
  // {
  //   path: '',
  //   component: GuestComponent,
  //   children: [{
  //     path: '',
  //     loadChildren: () => import('./modules/guest/guest.module').then(m => m.GuestModule)
  //   }]
  // }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
