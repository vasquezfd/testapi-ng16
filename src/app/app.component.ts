import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'alumno-app';

  constructor(private primeNGConfig: PrimeNGConfig){}

  ngOnInit(){
    this.primeNGConfig.ripple = true
  }
}
