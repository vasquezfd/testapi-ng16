export const environment = {
    production: true,
    apiUrl: "http://localhost:5209/api",
    apiNaciones: "https://restcountries.com/v3.1",
}
